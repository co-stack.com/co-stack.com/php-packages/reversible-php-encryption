## Show this help
help:
	echo "$(EMOJI_interrobang) Makefile version $(VERSION) help "
	echo ''
	echo 'About this help:'
	echo '  Commands are ${BLUE}blue${RESET}'
	echo '  Targets are ${YELLOW}yellow${RESET}'
	echo '  Descriptions are ${GREEN}green${RESET}'
	echo ''
	echo 'Usage:'
	echo '  ${BLUE}make${RESET} ${YELLOW}<target>${RESET}'
	echo ''
	echo 'Targets:'
	awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")+1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-${TARGET_MAX_CHAR_NUM}s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

install:
	mkdir -p $$HOME/.phive/
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php:7.2-fpm composer1 install
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php:7.2-fpm phive install --force-accept-unsigned --trust-gpg-keys CF1A108D0E7AE720,31C7E470E2138192,4AA394086372C20A,12CE0F1D262429A5,C00543248C87FB13

bash:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php-dev:7.2-fpm bash

qa: qa-phpmd qa-phpstan qa-phpcs qa-phpcpd qa-psalm qa-phpunit qa-infection

qa-phpmd:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php:7.2-fpm .project/phars/phpmd src,tests ansi .phpmd.xml

qa-phpstan:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php:7.2-fpm .project/phars/phpstan analyse -c .phpstan.neon

qa-phpcs:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php:7.2-fpm .project/phars/phpcs

qa-phpcpd:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php:7.2-fpm .project/phars/phpcpd --fuzzy --progress --ansi src/

qa-psalm:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php:7.2-fpm .project/phars/psalm --config=.psalm.xml

qa-phpunit:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php-dev:7.2-fpm vendor/bin/phpunit

qa-infection:
	docker run --rm -it -u1000:1000 -v "$$PWD":/app -v "$$HOME/.composer/":/tmp/composer/ -v "$$HOME/.phive/":/tmp/phive/ in2code/php-dev:7.2-fpm vendor/bin/infection --configuration=.infection.json -j$$(nproc) -s --min-covered-msi=100

# SETTINGS
MAKEFLAGS += --silent
SHELL := /bin/bash
VERSION := 1.0.0

# COLORS
RED     := $(shell tput -Txterm setaf 1)
GREEN   := $(shell tput -Txterm setaf 2)
YELLOW  := $(shell tput -Txterm setaf 3)
BLUE    := $(shell tput -Txterm setaf 4)
MAGENTA := $(shell tput -Txterm setaf 5)
CYAN    := $(shell tput -Txterm setaf 6)
WHITE   := $(shell tput -Txterm setaf 7)
RESET   := $(shell tput -Txterm sgr0)

# EMOJIS (some are padded right with whitespace for text alignment)
EMOJI_interrobang := "⁉️ "
