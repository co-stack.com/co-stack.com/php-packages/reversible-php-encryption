# co-stack/reversible-php-encryption - Reversible symmetric encryption based on defuse/php-encryption

[![pipeline status](https://gitlab.com/co-stack.com/co-stack.com/php-packages/reversible-php-encryption/badges/master/pipeline.svg)](https://gitlab.com/co-stack.com/co-stack.com/php-packages/reversible/-/commits/master)
[![coverage report](https://gitlab.com/co-stack.com/co-stack.com/php-packages/reversible-php-encryption/badges/master/coverage.svg)](https://gitlab.com/co-stack.com/co-stack.com/php-packages/reversible/-/commits/master)

## What is a reversible function?

Please see [co-stack/reversible](https://packagist.org/packages/co-stack/reversible) for more information about reversibles.

## About

This package is an extension to co-stack/reversible. It contains a `SymmetricEncryption` operation to safely
encrypt and decrypt a value.
The security of the `SymmetricEncryption` is based on an external package called
[defuse/php-encryption](https://packagist.org/packages/defuse/php-encryption), which provides an easier and safer way to interact
with PHP's encryption functions.

You can use this package as a safer, drop-in-replacement for the `SymmetricEncryption` provided by
[co-stack/reversible](https://packagist.org/packages/co-stack/reversible)
