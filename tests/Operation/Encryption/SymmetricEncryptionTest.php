<?php

declare(strict_types=1);

namespace CoStack\ReversiblePhpEncryptionTests\Operation\Encryption;

use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\ReversiblePhpEncryption\Operation\Encryption\SymmetricEncryption;
use PHPUnit\Framework\TestCase;

use function ctype_xdigit;
use function str_split;

/**
 * @coversDefaultClass \CoStack\ReversiblePhpEncryption\Operation\Encryption\SymmetricEncryption
 */
class SymmetricEncryptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricEncryptionCanEncryptEndDecryptString(): void
    {
        $value = 'My super secret string';

        $symmetricEncryption = new SymmetricEncryption('My super sercet password');
        $intermediate = $symmetricEncryption->execute($value);

        $this->assertStringNotContainsStringIgnoringCase($value, $intermediate);
        $this->assertStringNotContainsStringIgnoringCase('`', $intermediate);

        $actual = $symmetricEncryption->reverse($intermediate);

        $this->assertSame($value, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\DecryptionFailedException::__construct
     * @uses \CoStack\Reversible\Operation\OpenSSL::collectOpenSslErrors
     */
    public function testSymmetricEncryptionThrowsExceptionIfDecryptionSecretIsWrong(): void
    {
        $value = 'My super secret string';

        $symmetricEncryption = new SymmetricEncryption('My super secret encryption key');
        $intermediate = $symmetricEncryption->execute($value);

        $this->assertStringNotContainsStringIgnoringCase($value, $intermediate);

        $this->expectException(DecryptionFailedException::class);

        $symmetricEncryption = new SymmetricEncryption('My super wrong encryption key');
        $symmetricEncryption->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testSymmetricEncryptionReturnsHex(): void
    {
        $value = 'My super secret string';

        $symmetricEncryption = new SymmetricEncryption('My super secret encryption key', false);
        $intermediate = $symmetricEncryption->execute($value);

        $this->assertTrue(ctype_xdigit($intermediate));
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testSymmetricEncryptionReturnsNonHexIfSetTo(): void
    {
        $value = 'My super secret string';

        $symmetricEncryption = new SymmetricEncryption('My super secret encryption key', true);
        $intermediate = $symmetricEncryption->execute($value);

        $this->assertFalse(ctype_xdigit($intermediate));
    }
}
