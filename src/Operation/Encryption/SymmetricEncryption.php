<?php

declare(strict_types=1);

namespace CoStack\ReversiblePhpEncryption\Operation\Encryption;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\Reversible\Operation\OpenSSL;
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;

/**
 * @method string execute($value)
 * @method string reverse($value)
 */
class SymmetricEncryption extends AbstractReversible
{
    use OpenSSL;

    /** @var string */
    private $secret;

    /** @var bool */
    private $binary;

    /**
     * @param string $secret The secret string which is used to encrypt and decrypt the message
     * @param bool $binary Set to true if you want to handle encoding of the encrypted values yourself.
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function __construct(string $secret, bool $binary = false)
    {
        $this->secret = $secret;
        $this->binary = $binary;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $string): string {
            return Crypto::encryptWithPassword($string, $this->secret, $this->binary);
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $string): string {
            try {
                return Crypto::decryptWithPassword($string, $this->secret, $this->binary);
            } catch (WrongKeyOrModifiedCiphertextException $exception) {
                throw new DecryptionFailedException($this->collectOpenSslErrors(), $exception);
            }
        };
    }
}
